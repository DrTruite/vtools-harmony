function check_timeline_length() {
	current_stop_frame = scene.getStopFrame();
	new_stop_frame = scene.getStopFrame();
	drawing_columns_names = column.getDrawingColumnList()

	for (i=0; i<drawing_columns_names.length; i++) {
		for (j=current_stop_frame; j>=0; j--) {
			if (column.getTimesheetEntry(drawing_columns_names[i], 1, j).emptyCell == false) {
				new_stop_frame = Math.min(j, new_stop_frame);
				break;
			}
		}
	}

	if (new_stop_frame == current_stop_frame) {
		MessageBox.information("Timeline stop is set correctly at frame " + new_stop_frame);
	}
	else {
		MessageBox.information("Timeline stops at the wrong frame. It should stop at frame " + new_stop_frame + " instead of frame " + current_stop_frame);
	}
	
}

function check_palettes() {
	var numPaletteLists = PaletteObjectManager.getNumPaletteLists();
	var palettes_outside_env = [];
	var palettes_outside_env_names = [];
	for(var i=0; i < numPaletteLists; ++i) {
		var paletteList = PaletteObjectManager.getPaletteListByIndex(i);
		if (paletteList.isLoaded()) {
			for(var j=0; j < paletteList.numPalettes; ++j) {
				var palette = paletteList.getPaletteByIndex(j);
				if (palette.location != PaletteObjectManager.Constants.Location.ENVIRONMENT) {
					MessageLog.trace(palette.id);
					palettes_outside_env.push(palette.id);
					palettes_outside_env_names.push(palette.getName());
				}
			}
		}
	}

	var message_string = ""
	for (var i=0; i<palettes_outside_env.length; i++) {
		message_string += (palettes_outside_env_names[i] + " - ")
	}

	MessageBox.information(message_string + "should be removed");
	
}

check_palettes();