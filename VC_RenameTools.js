function vRename_find_and_replace() {
    /* 
	Find string and replace in selected nodes name
  */

    // Get selected nodes
    var selected_nodes = selection.selectedNodes()
    
    // Window properties
    var vRDialog = new Dialog();
    vRDialog.title = "vRename Find and Replace";
    vRDialog.width = 500;
    
    // Find and Replace
    var findAndReplaceGrp = new GroupBox();
    findAndReplaceGrp.title = "Find and Replace"
    
    var findLineEdit = new LineEdit();
    findLineEdit.label = "Find";
    findAndReplaceGrp.add(findLineEdit);
    findAndReplaceGrp.newColumn();
    var replaceLineEdit = new LineEdit();
    replaceLineEdit.label = "Replace";
    findAndReplaceGrp.add(replaceLineEdit);
    /*var findAndReplaceBtn = new Button();
    findAndReplaceBtn.label = "Find and Replace";
    findAndReplaceBtn.callback = "findAndReplace";
    findAndReplaceGrp.add(findAndReplaceBtn);*/
    
    // Construct Window
    vRDialog.add(findAndReplaceGrp);
    vRDialog.okButtonText = "Find and Replace";
    if (vRDialog.exec()) {
        for (i=0; i<selected_nodes.length; i++) {
            var newName = node.getName(selected_nodes[i]);
            MessageLog.trace(newName);
            newName = newName.replace(findLineEdit.text, replaceLineEdit.text);
            node.rename(selected_nodes[i], newName);
        }
    }
}

function vRename_auto_rename() {
    /* 
	Auto rename selected nodes according to their first child
  */

	// Get selected nodes
    var selected_nodes = selection.selectedNodes()
	var new_name = "";

	// Renaming from type
	for (i=0; i<selected_nodes.length; i++) {
		if ( node.type(selected_nodes[i]) == "StaticConstraint" ) {
			new_name = node.getName(node.dstNode(selected_nodes[i], 0, 0));
			new_name = "St-Tr_" + new_name.slice(0, new_name.length-2);
			node.rename(selected_nodes[i], new_name);
		}
		else if (node.type(selected_nodes[i]) == "PEG") {
			if (node.type(node.dstNode(selected_nodes[i], 0, 0)) == "READ") {
				new_name = node.getName(node.dstNode(selected_nodes[i], 0, 0)) + "-P";
				node.rename(selected_nodes[i], new_name);
			}
			else if (node.type(node.dstNode(selected_nodes[i], 0, 0)) == "GROUP" && node.type(node.dstNode(node.dstNode(selected_nodes[i], 0, 0), 0, 0)) == "READ") {
				new_name = node.getName(node.dstNode(node.dstNode(selected_nodes[i], 0, 0), 0, 0)) + "-P";
				node.rename(selected_nodes[i], new_name);
			}
		}
		else if (node.type(selected_nodes[i]) == "GROUP") {
			if (node.getName(selected_nodes[i]).search("Deformation") == 0) {
				new_name = node.getName(selected_nodes[i]).replace("Deformation", "Def");
				node.rename(selected_nodes[i], new_name);
			}
			else if (node.getName(selected_nodes[i]).search("Def-") == 0) {
				new_name = "Def-" + node.getName(node.dstNode(selected_nodes[i], 0, 0));
				node.rename(selected_nodes[i], new_name);
			}
			else if (node.type(node.subNode(selected_nodes[i], 2)) == "OffsetModule") {
				new_name = "Def-" + node.getName(node.dstNode(selected_nodes[i], 0, 0));
				node.rename(selected_nodes[i], new_name);
			}
			else {
				MessageLog.trace(node.type(node.subNode(selected_nodes[i], 2)));
			}
		}
		else if (node.type(selected_nodes[i]) == "DISPLAY") {
				new_name = node.getName(node.srcNode(selected_nodes[i], 0, 0)).slice(0, -2);
				node.rename(selected_nodes[i], new_name);
		}
		else if (node.type(selected_nodes[i]) == "COMPOSITE") {
				new_name = node.getName(node.srcNode(selected_nodes[i], 0, 0)).slice(0, -2) + "-C";
				node.rename(selected_nodes[i], new_name);
		}
		else if (node.type(selected_nodes[i]) == "ImageSwitch") {
				new_name = "Im-Sw_" + node.getName(node.srcNode(node.srcNode(selected_nodes[i], 0, 0), 0, 0));
				MessageLog.trace(node.flatSrcNode(node.srcNode(selected_nodes[i], 0), 0));
				node.rename(selected_nodes[i], new_name);
		}
		else if (node.type(selected_nodes[i]) == "COLOR_OVERRIDE_TVG") {
				new_name = "CoOv_" + node.getName(node.srcNode(selected_nodes[i],0, 0)).slice(0, -2);  
				node.rename(selected_nodes[i], new_name);
		}
		else {
			MessageLog.trace(node.type(selected_nodes[i]));
		}
	}
}
