function moveDrawing() {
  var sNode = selection.selectedNode(0);
  if (node.type(sNode) == "READ")
  {
    var myAttr = node.getAttrList(sNode, frame.current(), "");
    var position = new Point3d(5,10,0);
    for(i=0; i < myAttr.length; i++)
    {
      if(myAttr[i].name() == "Position")
      {
        myAttr[i].setValue(position);
      }
    }
  }
}
//moveDrawing()


function list_attributes() {
	var n=selection.selectedNode(0); // node selected
	var myList = node.getAttrList( n, 1); // attributes list

	// boucle pour lister les noms des attributs
	for (i=0;i<myList.length;i++){
		MessageLog.trace(myList[i].keyword());
	}
}


function isolate_selection() {
	scene.beginUndoRedoAccum("Toggle isolate");

	// Get nodes to toggle
	var scene_drawings = node.getNodes(["READ"]);
	var selected_nodes = selection.selectedNodes();

	// Check if any drawing in disabled
	var should_hide = true;
	for(i=0; i<scene_drawings.length; i++) {
		if(node.getEnable(scene_drawings[i]) == false) {
			should_hide = false;
		}
	}

	// If any drawing is disabled, activate them all
	if(should_hide == false) {
		for(i=0; i<scene_drawings.length; i++) {
			node.setEnable(scene_drawings[i], true);
		}
	}
	else { // Else disable any not selected drawings
		// Remove selected nodes from list
		for(i=0; i<selected_nodes.length; i++) {
			if(scene_drawings.indexOf(selected_nodes[i]) != -1) {
				scene_drawings.splice(scene_drawings.indexOf(selected_nodes[i]), 1);
			}
		}
		// Hide nodes
		for(i=0; i<scene_drawings.length; i++) {
			node.setEnable(scene_drawings[i], false);
		}
	}	
	
	scene.endUndoRedoAccum();
}
//isolate_selection();


function get_attribute_value(att) {
	switch(att.typeName()) {
		case "PUSH_BUTTON" :
		case "BOOL" : 
			MessageLog.trace(att.name() + " : " + att.boolValue());
			return att.boolValue();
			break;
		case "POSITION_3D" :
		case "ROTATION_3D" :
		case "SCALE_3D" :
			MessageLog.trace(att.name() + " : " + att.pos3dValue().x + att.pos3dValue().y + att.pos3dValue().z);
			return Point3d(att.pos3dValue().x, att.pos3dValue().y, att.pos3dValue().z);
			break;
		case "DOUBLE" :
			MessageLog.trace(att.name() + " : " + att.doubleValue());
			return att.doubleValue();
			break;
		default: MessageLog.trace("Error (002): Wrong Attribute type " + att.typeName());
	}
}
/*
class vNode {
	var view_x, view_y;
	var zero_x, zero_y, zero_z, zero_rot, zero_scale_x, zero_scale_y;
	var parent_node, primary_child;
	constructor(sNode){
		// Get node position in the node view
		[this.view_x, this.view_y] = [node.coordX(sNode), node.coordY(sNode)];
		// Get node local coordinates at frame 0
		if(node.type(sNode) == "StaticConstraint"){
			//this.zero_x = 
		}
		//TODO get inspiration from https://github.com/chrishcarter/harmony/blob/master/NC_TweenUI.js ?
		// Get node local rotation at frame 0
		//TODO this.rot = view_pos;
		// Get node local scale at frame 0
		//TODO [this.zero_scale_x, this.zero_scale_y, this.zero_scale_z] = zero_scale;
		// Get node parent and children
	}
}
*/

// Cosinus of an angle in degrees
function dcos(degree){
    return Math.cos(degree * (Math.PI/180))
}

// Sinus of an angle in degrees
function dsin(degree){
    return Math.sin(degree * (Math.PI/180))
}

function rotate_from_Euler(matrix, x, y, z){
    var tmat = matrix;
    var xaxis = new Vector3d()
    var yaxis = new Vector3d()
    var zaxis = new Vector3d()
    xaxis.setXYZ(1, 0, 0);
    yaxis.setXYZ(0, 1, 0);
    zaxis.setXYZ(0, 0, 1);
    tmat.rotateDegrees(x, xaxis);
    tmat.rotateDegrees(y, yaxis);
    tmat.rotateDegrees(z, zaxis);
    return tmat
}

function matrix_as_text(matrix){
    var txt = "";
    var space = " ";
    var ent = "\n";
    var ret = txt.concat(ent, matrix.m00.toPrecision(3).toString(), space, matrix.m01.toPrecision(3).toString(), space, matrix.m02.toPrecision(3).toString(), space,             matrix.m03.toPrecision(3).toString(), ent, matrix.m10.toPrecision(3).toString(), space, matrix.m11.toPrecision(3).toString(), space, matrix.m12.toPrecision(3).toString(), space, matrix.m13.toPrecision(3).toString(), ent, matrix.m20.toPrecision(3).toString(), space, matrix.m21.toPrecision(3).toString(), space, matrix.m22.toPrecision(3).toString(), space, matrix.m23.toPrecision(3).toString(), ent, matrix.m30.toPrecision(3).toString(), space, matrix.m31.toPrecision(3).toString(), space, matrix.m32.toPrecision(3).toString(), space,             matrix.m33.toPrecision(3).toString());
    return ret
}

function merge_statics2(){
	// Selected Nodes
	var selected_nodes = selection.selectedNodes();
    
    // Check Selected Nodes
    var only_two_nodes_selected = false;
    if(selected_nodes.length == 2) only_two_nodes_selected = true;
    
    var both_nodes_are_statics = false;
    if(node.type(selected_nodes[0]) == "StaticConstraint" && node.type(selected_nodes[1]) == "StaticConstraint") both_nodes_are_statics = true;
    
    var nodes_are_connected = false;
    if(node.srcNode(selected_nodes[0], 0) == selected_nodes[1] || node.srcNode(selected_nodes[1], 0) == selected_nodes[0]) nodes_are_connected = true;
    
    if(only_two_nodes_selected && both_nodes_are_statics && nodes_are_connected){
    
        // Put top node at index 0
        if(node.srcNode(selected_nodes[0], 0) == selected_nodes[1]){
            selected_nodes.push(selected_nodes[0]);
            selected_nodes.shift();
        }
        
        // Store transform values for easy access
        var node1 = {
            posx: 0, posy: 0, posx: 0,
            scalex: 1, scaley: 1, scalez: 1,
            rotx: 0, roty: 0, rotz: 0, 
            skewx: 0, skewy: 0, skewz: 0,        
        };
        var node2 = {
            pox: 0, posy: 0, posx: 0,
            scalex: 1, scaley: 1, scalez: 1,
            rotx: 0, roty: 0, rotz: 0, 
            skewx: 0, skewy: 0, skewz: 0,       
        };
        
        node1.posx = node.getAttrList(selected_nodes[0], 1, "translate")[1].doubleValue();
        node1.posy = node.getAttrList(selected_nodes[0], 1, "translate")[2].doubleValue();
        node1.posz = node.getAttrList(selected_nodes[0], 1, "translate")[3].doubleValue();
        node1.scalex = node.getAttrList(selected_nodes[0], 1, "scale")[3].doubleValue();
        node1.scaley = node.getAttrList(selected_nodes[0], 1, "scale")[4].doubleValue();
        node1.scalez = node.getAttrList(selected_nodes[0], 1, "scale")[5].doubleValue();
        node1.rotx = node.getAttrList(selected_nodes[0], 1, "rotate")[1].doubleValue();
        node1.roty = node.getAttrList(selected_nodes[0], 1, "rotate")[2].doubleValue();
        node1.rotz = node.getAttrList(selected_nodes[0], 1, "rotate")[3].doubleValue();
        node1.skewx = node.getAttrList(selected_nodes[0], 1)[6].doubleValue();
        node1.skewy = node.getAttrList(selected_nodes[0], 1)[7].doubleValue();
        node1.skewz = node.getAttrList(selected_nodes[0], 1)[8].doubleValue();
        
        node2.posx = node.getAttrList(selected_nodes[1], 1, "translate")[1].doubleValue();
        node2.posy = node.getAttrList(selected_nodes[1], 1, "translate")[2].doubleValue();
        node2.posz = node.getAttrList(selected_nodes[1], 1, "translate")[3].doubleValue();
        node2.scalex = node.getAttrList(selected_nodes[1], 1, "scale")[3].doubleValue();
        node2.scaley = node.getAttrList(selected_nodes[1], 1, "scale")[4].doubleValue();
        node2.scalez = node.getAttrList(selected_nodes[1], 1, "scale")[5].doubleValue();
        node2.rotx = node.getAttrList(selected_nodes[1], 1, "rotate")[1].doubleValue();
        node2.roty = node.getAttrList(selected_nodes[1], 1, "rotate")[2].doubleValue();
        node2.rotz = node.getAttrList(selected_nodes[1], 1, "rotate")[3].doubleValue();
        node2.skewx = node.getAttrList(selected_nodes[1], 1)[6].doubleValue();
        node2.skewy = node.getAttrList(selected_nodes[1], 1)[7].doubleValue();
        node2.skewz = node.getAttrList(selected_nodes[1], 1)[8].doubleValue();
        
        // Store transform values in matrices
        var matrix1 = new Matrix4x4();
        matrix1.translate(node1.posx, node1.posy, node1.posz);
        matrix1.scale(node1.scalex, node1.scaley, node1.scalez);
        //matrix1.setFromEulerAngles(node1.rotx, node1.roty, node1.rotz);
        matrix1 = rotate_from_Euler(matrix1, node1.rotx, node1.roty, node1.rotz);
        matrix1.skew(node1.skewx);
        
        var matrix2 = new Matrix4x4();
        matrix2.translate(node2.posx, node2.posy, node2.posz);
        matrix2.scale(node2.scalex, node2.scaley, node2.scalez);
        //matrix2.setFromEulerAngles(node2.rotx, node2.roty, node2.rotz);
        matrix2 = rotate_from_Euler(matrix2, node2.rotx, node2.roty, node2.rotz);
        matrix2.skew(node2.skewx);
        
        // Calculate new values
        var node3 = {
            posx: 0, posy: 0, posx: 0,
            scalex: 1, scaley: 1, scalez: 1,
            rotx: 0, roty: 0, rotz: 0, 
            skewx: 0, skewy: 0, skewz: 0,        
        };
        
        /*
        MessageLog.trace(node1.posx);
        MessageLog.trace(matrix1.extractPosition().x);
        MessageLog.trace(node1.rotx);
        MessageLog.trace(matrix1.extractRotation().x);
        MessageLog.trace(node1.scalex);
        MessageLog.trace(matrix1.extractScale().x);
        */
        
        matrix3 = matrix1
        MessageLog.trace(matrix_as_text(matrix1));
        MessageLog.trace(matrix_as_text(matrix2));
        MessageLog.trace(matrix_as_text(matrix3));
        //matrix3.multiplyEq(matrix2.getInverse());
        matrix3 = matrix1.multiply(matrix2);
        MessageLog.trace(matrix_as_text(matrix3));
        
        node3.posx = matrix3.extractPosition().x;
        node3.posy = matrix3.extractPosition().y;
        node3.posz = matrix3.extractPosition().z;
        node3.scalex = matrix3.extractScale().x;
        node3.scaley = matrix3.extractScale().y;
        node3.scalez = matrix3.extractScale().z;
        node3.rotx = matrix3.extractRotation().x;
        node3.roty = matrix3.extractRotation().y;
        node3.rotz = matrix3.extractRotation().z;
        node3.skewx = matrix3.extractSkew();
        
        /*
        node3.posx = node1.posx + (node2.posx * dcos(node1.rotz)) + (node2.posy * dcos(node1.rotz + 90));
        node3.posy = node1.posy + (node2.posx * dsin(node1.rotz)) + (node2.posy * dsin(node1.rotz + 90));
        node3.posz = node2.posz + node2.posz
        node3.scalex = node1.scalex * ((node2.scalex * dcos(node1.rotz)) + (node2.scaley * dcos(node1.rotz + 90)));
        node3.scaley = node1.scaley * ((node2.scalex * dcos(node1.rotz)) + (node2.scaley * dcos(node1.rotz + 90)));
        node3.rotz = node1.rotz + (node2.rotz * node1.scalex * node1.scaley);
        node3.skewx = node1.skewx + node2.skewx;
        */
        
        // Apply new values to Top Static
        
        scene.beginUndoRedoAccum("Merge Selected Statics");
        
        node.setTextAttr(selected_nodes[0], "translate.x", 0, node3.posx);
        node.setTextAttr(selected_nodes[0], "translate.y", 0, node3.posy);
        node.setTextAttr(selected_nodes[0], "translate.z", 0, node3.posz);
        node.setTextAttr(selected_nodes[0], "scale.x", 0, node3.scalex);
        node.setTextAttr(selected_nodes[0], "scale.y", 0, node3.scaley);
        node.setTextAttr(selected_nodes[0], "scale.z", 0, node3.scalez);
        node.setTextAttr(selected_nodes[0], "rotate.anglex", 0, node3.rotx);
        node.setTextAttr(selected_nodes[0], "rotate.angley", 0, node3.roty);
        node.setTextAttr(selected_nodes[0], "rotate.anglez", 0, node3.rotz);
        node.setTextAttr(selected_nodes[0], "skewx", 0, node3.skewx);
        node.setTextAttr(selected_nodes[0], "skewy", 0, node3.skewy);
        node.setTextAttr(selected_nodes[0], "skewz", 0, node3.skeyz);
        
        // Delete Bottom Static
        node.deleteNode(selected_nodes[1], true, true);
        scene.endUndoRedoAccum();
        
    }
}

function merge_statics() {
	// Selected Nodes
	var selected_nodes = selection.selectedNodes();
	var statics_to_merge = [];
	
	// Collect statics from selection
	for(i=0; i<selected_nodes.length; i++) {
		if(node.type(selected_nodes[i]) == "StaticConstraint") {
			statics_to_merge.push(selected_nodes[i]);
		}
	}

	// List attributes to merge in an array [[node1.att1, node1.att2, ...], [node2.att1, node2.att2, ...], ...]
	var static_attributes = []
	for(i=0; i<statics_to_merge.length; i++){
		static_attributes.push(node.getAttrList(statics_to_merge[i], 1));
	}

	scene.beginUndoRedoAccum("Merge Selected Statics");

	// Set new values to first static
	for(i=0; i<static_attributes[0].length; i++) {
		var new_value = get_attribute_value(static_attributes[0][i]);
		for(j=1; j<statics_to_merge.length; j++){
			switch(static_attributes[j][i].typeName()) {
				case "PUSH_BUTTON" :
				case "BOOL" : 
					new_value = new_value || get_attribute_value(static_attributes[j][i]);
					break;
				case "POSITION_3D" :
				case "ROTATION_3D" :
				case "SCALE_3D" :
					new_point = get_attribute_value(static_attributes[j][i]);
					new_value = new_value.add(Vector3d(new_point.x, new_point.y, new_point.z));
					break;
				case "DOUBLE" :
					new_value = new_value + get_attribute_value(static_attributes[j][i]);
					break;
				default: MessageLog.trace("Error (001): Wrong Attribute type " + att.typeName());
			}
		}
		if(typeof(new_value) === 'object'){
			MessageLog.trace("NEW_VALUE : " + new_value.x + new_value.y + new_value.z);
			MessageLog.trace(node.getAttr(statics_to_merge[0], frame.current(), static_attributes[0][i].name()));
			node.setTextAttr(statics_to_merge[0], static_attributes[0][i].name().toLowerCase() + ".x", frame.current(), new_value.x);
			node.setTextAttr(statics_to_merge[0], static_attributes[0][i].name().toLowerCase() + ".y", frame.current(), new_value.y);
			node.setTextAttr(statics_to_merge[0], static_attributes[0][i].name().toLowerCase() + ".z", frame.current(), new_value.z);
		}
		else {
			MessageLog.trace(typeof(new_value));
			node.setTextAttr(statics_to_merge[0], static_attributes[0][i].name(), frame.current(), new_value);
		}
	}

	// Delete other statics
	for(i=1; i<statics_to_merge.length; i++) {
		node.deleteNode(statics_to_merge[i], true, true);
	}

	scene.endUndoRedoAccum();
}
